

import Foundation

public class UserResponse {
	public var user : User?
	public var device_type : String?
	public var device_token : String?
	public var token : String?


    public class func modelsFromDictionaryArray(array:Array<Dictionary<String,Any>>) -> [UserResponse]
    {
        var models:[UserResponse] = []
        for item in array
        {
            models.append(UserResponse(dictionary: item)!)
        }
        return models
    }


	required public init?(dictionary: Dictionary<String,Any>) {

		if (dictionary["user"] != nil) {
            user = User(dictionary: dictionary["user"] as! Dictionary<String,Any>)
        }
		device_type = dictionary["device_type"] as? String
		device_token = dictionary["device_token"] as? String
		token = dictionary["token"] as? String
	}


	public func dictionaryRepresentation() -> Dictionary<String,Any> {

		let dictionary = NSMutableDictionary()

		dictionary.setValue(self.user?.dictionaryRepresentation(), forKey: "user")
		dictionary.setValue(self.device_type, forKey: "device_type")
		dictionary.setValue(self.device_token, forKey: "device_token")
		dictionary.setValue(self.token, forKey: "token")

		return dictionary as! Dictionary<String,Any>
	}

}
