

import Foundation
import UIKit



extension NSNotification.Name {
    static let stockPriceChnage = Notification.Name("stockPriceChnage")
}

struct Token {
    static let X_Finnhub_Token = "bst4jan48v6ol9tkqa00"
}

struct keys {
    
    struct userdefaults {
        static let isStockTableExist    = "isStockTableExist"
        static let pushToken            = "pushtoken"
    }
}

struct ReuseIdentifiers {
    static let StockCell = "StockCell"
    static let NewsCell = "NewsCell"
    static let NewsPreviewCell = "NewsPreviewCell"
    static let SearchCell = "SearchCell"
}


struct Strings {
    static let placeholderImage = "/image/d44e081ffbc3dfdcfaea6cddd3009fc6/1592511010_f8053b84bbe92de8e50f.jpg"
}

struct Colors {
   
    struct red {
        static let primary = "#FB4B4E"
        static let secondary = "#15212E"
    }
    
    struct green {
        static let primary = "#3BFC55"
        static let secondary = "#15212E"
    }
    
    static let inputBg = "#1D314D"
    static let gray = "#6A7D97"
    static let primary = "#15212E"
    static let link = "#697DFB"
}
