
import Foundation
import UIKit


class CameraHandler: NSObject{
    
    static let shared = CameraHandler()
    
    fileprivate var target: UIViewController?
    fileprivate var onSuccess: ((UIImage?) -> Void)?
    
    func openPicker(with sourceType:UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = sourceType
            target?.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func showActionSheet(on vc: UIViewController, with completion:((UIImage?) -> Void)?) {
        
        target = vc
        onSuccess = completion
        
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.openPicker(with: .camera)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { (alert:UIAlertAction!) -> Void in
            self.openPicker(with: .photoLibrary)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
    }
    
}


extension CameraHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        target?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        target?.dismiss(animated: true, completion: {
            let image = info[.originalImage] as? UIImage
            self.onSuccess?(image)
        })
    }
    
}
