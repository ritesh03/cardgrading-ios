//
//  RoundCornerButton.swift
//  CardReader
//
//

import Foundation
import UIKit

@IBDesignable public class RoundCornerButton: UIButton {
    
    @IBInspectable var borderColor: UIColor = UIColor.white{
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    @IBInspectable var titleColor: UIColor = UIColor.white{
        didSet {
            self.titleColor = UIColor.black
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 2.0{
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = self.cornerRadius
            layer.masksToBounds = self.cornerRadius > 0
        }
    }
    
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}

