

import UIKit
import Foundation

struct defaultKeys {
    static let kuserDetail   =   "userDetail"
    static let ktoken        =   "token"
    static let kdeviceToken  =   "deviceToken"
}

class DefaultUtils: NSObject {
    static let userDefaults = UserDefaults.standard
    
    class func setUserData(data: [String : Any]){
        userDefaults.setValue(data, forKey: defaultKeys.kuserDetail)
    }
    
//    class func getUserData() -> User? {
//        if let userDict = userDefaults.value(forKey: defaultKeys.kuserDetail) as? [String:Any] {
//            return User(dictionary: userDict)
//        }
//        return nil
//    }
    
    class func getUserDataDict() -> [String:Any]? {
        return userDefaults.value(forKey: defaultKeys.kuserDetail) as? [String:Any]
    }
    
    class func removeUserData() {
        userDefaults.removeObject(forKey: defaultKeys.kuserDetail)
    }
    
    
    class func saveToken(token: String?){
        userDefaults.setValue(token, forKey: defaultKeys.ktoken)
    }
    
    class func getToken() -> String? {
        return userDefaults.value(forKey: defaultKeys.ktoken) as? String ?? ""
    }
    
    class func removeToken(){
        userDefaults.removeObject(forKey: defaultKeys.ktoken)
    }
    
    
    class func saveDeviceToken(deviceToken: String?) {
        userDefaults.setValue(deviceToken, forKey: defaultKeys.kdeviceToken)
    }
    
    class func getDeviceToken() -> String? {
        return userDefaults.value(forKey: defaultKeys.kdeviceToken) as? String ?? "Simulator"
    }
    class func removeDeviceToken() {
        userDefaults.removeObject(forKey: defaultKeys.kdeviceToken)
    }
}
