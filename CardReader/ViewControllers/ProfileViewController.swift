//
//  ProfileViewController.swift
//  CardReader
//
//

import UIKit

class ProfileViewController: BaseViewController {
    @IBOutlet weak var firsNameTxtField: UITextField!
    @IBOutlet weak var lastNametxtField: UITextField!
    @IBOutlet weak var emailTxtField: UITextField!
    @IBOutlet weak var btnForEdit: UIButton!
    @IBOutlet weak var btnForSubmit: RoundCornerButton!
    @IBOutlet weak var btnForCancel: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firsNameTxtField.text = AppInstance.shared.user?.user?.first_name ?? ""
        lastNametxtField.text = AppInstance.shared.user?.user?.last_name ?? ""
        emailTxtField.text = AppInstance.shared.user?.user?.email ?? ""
        btnForSubmit.isHidden = true
        btnForCancel.isHidden = true
        firsNameTxtField.isUserInteractionEnabled = false
        lastNametxtField.isUserInteractionEnabled = false
        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
    }
    
    @IBAction func editAction (_ sender:UIButton) {
        btnForSubmit.isHidden = false
        btnForCancel.isHidden = false
        btnForEdit.isHidden = true
        firsNameTxtField.isUserInteractionEnabled = true
        lastNametxtField.isUserInteractionEnabled = true
    }
    
    @IBAction func submitAction (_ sender:UIButton) {
        
        guard let firstName = self.firsNameTxtField.text, !firstName.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter first name")
        }
        
        guard let lastName = self.lastNametxtField.text, !lastName.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter last name")
        }

        UserService().updateProfile(first_name: firstName, last_name: lastName) { (result) in
            if result != nil{
                if let dict = result as? Dictionary<String,Any>{
                    SwiftAlert.show(self, title: "", message: dict["message"] as? String ?? "") { (index) in
                        AppInstance.shared.user?.user?.first_name = firstName
                        AppInstance.shared.user?.user?.last_name = lastName
                        DefaultUtils.setUserData(data:  AppInstance.shared.user?.dictionaryRepresentation() ?? ["":""])
                        self.btnForSubmit.isHidden = true
                        self.btnForCancel.isHidden = true
                        self.btnForEdit.isHidden = false
                        self.firsNameTxtField.isUserInteractionEnabled = false
                        self.lastNametxtField.isUserInteractionEnabled = false
                    }
                }
            }
        }
        
    }
    
    @IBAction func actionForCancel(_ sender: Any) {
        btnForSubmit.isHidden = true
        btnForCancel.isHidden = true
        btnForEdit.isHidden = false
        firsNameTxtField.isUserInteractionEnabled = false
        lastNametxtField.isUserInteractionEnabled = false
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
