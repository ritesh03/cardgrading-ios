
//
//  BaseViewController.swift
//  Stonks
//
//  Created by Ritesh on 14/08/20.
//  Copyright © 2020 Ritesh. All rights reserved.
//

import UIKit

//extension UINavigationController {
//    open override var preferredStatusBarStyle: UIStatusBarStyle {
//        return topViewController?.preferredStatusBarStyle ?? .default
//    }
//}

class BaseViewController: UIViewController {
    
    private var vSpinner:SwiftSpinner?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func popAction(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    func showLoader() {
        if let window = UIApplication.shared.windows.first {
            let spinnerView = SwiftSpinner(frame: window.bounds)
            DispatchQueue.main.async {
                window.addSubview(spinnerView)
            }
            vSpinner = spinnerView
        }
        
    }
    
    func hideLoader() {
        
        DispatchQueue.main.async {
            self.vSpinner?.removeFromSuperview()
            self.vSpinner = nil
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension BaseViewController {
    
    // MARK: - Show default alert message
    func showAlert(with message:String) {
        SwiftAlert.show(self, title: "", message: message)
    }
    
    // MARK: - Add back button with custom image
    func addBackButton(with name:String = "back_arrow") {
        let backButtonImage = UIImage(named: name)?.withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        let leftBarButtonItem = UIBarButtonItem(image: backButtonImage,
                                                style: .plain,
                                                target: self,
                                                action: #selector(onLeftBarButtonClicked(_ :)))
        navigationItem.leftBarButtonItem = leftBarButtonItem
    }
    
    // MARK: - Override this function if want to change back button behaviour
    @objc func onLeftBarButtonClicked(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func hideBackButton() {
        self.navigationItem.hidesBackButton = true
    }
    
    func hideNavigationBar(_ hide: Bool, animated: Bool = true) {
        self.navigationController?.setNavigationBarHidden(hide, animated: animated)
    }
}
