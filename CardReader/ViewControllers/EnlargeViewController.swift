//
//  EnlargeViewController.swift
//  CardReader
//
//  Created by Ritesh Chopra on 3/2/21.
//

import UIKit

class EnlargeViewController: BaseViewController {

    class func instantiateViewController() -> EnlargeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: EnlargeViewController.self)) as! EnlargeViewController
        return vc
    }
    @IBOutlet weak var viewForTranslucent: UIView!
    
    @IBOutlet weak var viewForImage: UIView!
    @IBOutlet weak var btnForCross: UIButton!
    @IBOutlet weak var imgView: UIImageView!
    typealias typeCompletionHandler = (Int?) -> ()
    var complition : typeCompletionHandler = { _  in }
    func dismissVCCompletion(_ completionHandler: @escaping typeCompletionHandler) {
        self.complition = completionHandler
    }
    var image:UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewForImage.layer.cornerRadius = 16
        viewForImage.layer.masksToBounds = true
        imgView.layer.cornerRadius = 16
        imgView.layer.masksToBounds = true
        btnForCross.layer.cornerRadius = 25
        btnForCross.layer.masksToBounds = true
        
        if let img = image{
            self.imgView.image = img
        }

        // Do any additional setup after loading the view.
    }
    
    @IBAction func actionForCross(_ sender: Any) {
        self.dismissView(selection: 0)
    }
    private func dismissView(selection:Int) {
        self.view.removeFromSuperview()
        removeFromParent()
        complition(selection)
    }

}
