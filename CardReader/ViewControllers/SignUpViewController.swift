//
//  SignUpViewController.swift
//  CardReader
//
//

import Foundation
import UIKit

class SignUpViewController: BaseViewController {
    
    class func instantiateViewController() -> SignUpViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: SignUpViewController.self)) as! SignUpViewController
        return vc
    }
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
        self.autologin()
    }
    
    func autologin(){
        if let dict = DefaultUtils.getUserDataDict(){
            print(dict)
            let user = UserResponse(dictionary: dict)
            AppInstance.shared.user  = user
            let vc = CustomrTabController.instantiateViewController()
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
    }
    
    @IBAction func signUpAction(_ sender:UIButton) {
        
        guard let firstname = self.firstNameTextField.text, !firstname.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter first name")
        }
        guard let lastname = self.lastNameTextField.text, !lastname.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter last name")
        }
        
        guard let email = self.emailTextField.text, !email.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter email")
        }
        
        guard Validator.email(email).validate else {
            return SwiftAlert.show(self, title: "", message: "Please enter valid email")
        }
        
        guard let password = self.passwordTextField.text, !password.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter password")
        }

        UserService().signUp(username: email, email: email, password: password, first_name: firstname, last_name: lastname, device_type: "ios", device_token: "") { (result) in
            self.hideLoader()
            if (result != nil){
                if let dict = result as? Dictionary<String,Any> {
                    if let data = dict["data"] as? Dictionary<String,Any> {
                        if let user = UserResponse(dictionary: data) {
                            AppInstance.shared.user = user
                            DefaultUtils.setUserData(data: user.dictionaryRepresentation())
                            DispatchQueue.main.async {
                                SwiftAlert.show(self, title: "", message: dict["message"] as? String ?? "") { (index) in
                                    let vc = CustomrTabController.instantiateViewController()
                                    self.navigationController?.pushViewController(vc, animated: false)
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func loginAction(_ sender:UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
