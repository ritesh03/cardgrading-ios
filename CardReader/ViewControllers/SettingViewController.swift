//
//  SettingViewController.swift
//  CardReader
//
//

import UIKit

class SettingViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
    }
    
    @IBAction func chnagePasswordAction (_ sender:UIButton) {
        let vc = ChangePasswordViewController.instantiateViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func logOutAction (_ sender:UIButton) {
       
        SwiftAlert.show(self, title: "", message: "Are you sure you want to logout?", preferredStyle: .actionSheet, options: [AlertAction("Logout", .destructive), AlertAction("Cancel", .cancel)]) { (index) in
            switch index {
            case 0:
                UserService().logOut { (result) in
                    DispatchQueue.main.async {
                        DefaultUtils.removeUserData()
                        self.tabBarController?.navigationController?.popToRootViewController(animated: true)
                    }
                }
                
            default:
                break
            }
           
            
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
