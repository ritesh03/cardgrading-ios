//
//  ViewController.swift
//  CardReader
//
//  Created by Ritesh Chopra on 30/01/21.
//

import UIKit

class ViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
        self.autologin()
    }

    func autologin(){
        if let dict = DefaultUtils.getUserDataDict(){
            print(dict)
            let user = UserResponse(dictionary: dict)
            AppInstance.shared.user  = user
            let vc = LoginViewController.instantiateViewController()
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
    }
    
    
    @IBAction func signUpAction(_ sender:UIButton) {
        let vc = SignUpViewController.instantiateViewController()
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func loginAction(_ sender:UIButton) {
        let vc = LoginViewController.instantiateViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

}

