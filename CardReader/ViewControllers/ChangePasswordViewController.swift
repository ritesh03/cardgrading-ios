//
//  ChangePasswordViewController.swift
//  CardReader
//
//

import UIKit

class ChangePasswordViewController: BaseViewController {

    @IBOutlet weak var oldPasswordTextField: UITextField!
    @IBOutlet weak var newPasswordTextField: UITextField!
    @IBOutlet weak var cnfNewPasswordTextField: UITextField!
    
    class func instantiateViewController() -> ChangePasswordViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ChangePasswordViewController.self)) as! ChangePasswordViewController
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
    }
    
    @IBAction func submitAction (_ sender:UIButton) {
        
        guard let oldPassword = self.oldPasswordTextField.text, !oldPassword.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter old password")
        }
        
        guard let newpassword = self.newPasswordTextField.text, !newpassword.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter new password")
        }
        guard let cnfNewPassword = self.cnfNewPasswordTextField.text, !cnfNewPassword.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter confirm password")
        }
        UserService().changePassword(old_password: oldPassword, new_password: newpassword) { (result) in
            if result != nil{
                if let dict = result as? Dictionary<String,Any>{
                    SwiftAlert.show(self, title: "", message: dict["message"] as? String ?? "") { (index) in
                        self.navigationController?.popViewController(animated: true)
                    }
                }
            }
        }
    }
}
