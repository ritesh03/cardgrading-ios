//
//  LoginViewController.swift
//  CardReader
//
//

import Foundation
import UIKit

class LoginViewController: BaseViewController {

    class func instantiateViewController() -> LoginViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: LoginViewController.self)) as! LoginViewController
        return vc
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
        self.autologin()
    }
    func autologin(){
        if let dict = DefaultUtils.getUserDataDict(){
            print(dict)
            let user = UserResponse(dictionary: dict)
            AppInstance.shared.user  = user
            let vc = CustomrTabController.instantiateViewController()
            self.navigationController?.pushViewController(vc, animated: false)
            return
        }
    }
    @IBAction func forgetAction(_ sender:UIButton) {
        let vc = ForgotViewController.instantiateViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func loginAction(_ sender:UIButton) {
        
        guard let email = self.emailTextField.text, !email.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter email")
        }
        
        guard Validator.email(email).validate else {
            return SwiftAlert.show(self, title: "", message: "Please enter valid email")
        }
        
        guard let password = self.passwordTextField.text, !password.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter password")
        }

        UserService().login(username: email, password: password) { (result) in
        
            if result != nil {
                DispatchQueue.main.async {
                    AppInstance.shared.user = result
                    DefaultUtils.setUserData(data: result?.dictionaryRepresentation() ?? ["":""])
                    let vc = CustomrTabController.instantiateViewController()
                    self.navigationController?.pushViewController(vc, animated: false)
                }
            }
        }
    }
}
