//
//  HomeViewController.swift
//  CardReader
//
//

import UIKit

class HomeViewController: BaseViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblForAddPicture: UILabel!
    @IBOutlet weak var lblForDetail: UILabel!
    @IBOutlet weak var tblView: UITableView!
    var dictForDetails = Dictionary<String,Any>()
    @IBOutlet weak var collectionViewForFront: UICollectionView!
    @IBOutlet weak var collectionViewForBack: UICollectionView!
    var arrForFrontImage = [UIImage]()
    var arrForBackImage = [UIImage]()
    @IBOutlet weak var btnForUpload: UIButton!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tblView.isHidden = true
        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
        btnForUpload.layer.cornerRadius = 22
        btnForUpload.layer.masksToBounds = true
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.resetData()
        
    }
    
    func resetData() {
        self.dictForDetails.removeAll()
        self.tblView.reloadData()
        self.tblView.isHidden = true
        self.lblForDetail.isHidden = false
        self.lblForAddPicture.isHidden = false
    }
    @IBAction func addAction (_ sender:UIButton) {
        
        
        if arrForFrontImage.count == 0{
            return SwiftAlert.show(self, title: "", message: "Please select front photos.")
        }
        if arrForBackImage.count == 0{
            return SwiftAlert.show(self, title: "", message: "Please select back photos.")
        }
        self.lblForAddPicture.isHidden = false
        self.lblForDetail.isHidden = false
        self.tblView.isHidden = true
        self.dictForDetails.removeAll()
        self.tblView.reloadData()
        
        UserService().uploadFile(images: arrForFrontImage, imagesBack: arrForBackImage) { (result) in
            DispatchQueue.main.async {
                if let dict = result as? Dictionary<String,Any>,
                   let imageId = (dict["data"] as? Dictionary<String,Any>)?["image_id"] as? Int {
                    self.fetchImageDetails(image_id: "\(imageId)")
                }
            }
        }
    }
    private  func fetchImageDetails(image_id:String){
        UserService().imageDetail(image_id: image_id) { (result) in
            DispatchQueue.main.async {
                if let dict = result as? Dictionary<String,Any>,
                   let data = dict["data"] as? Dictionary<String,Any>{
                    self.dictForDetails = data
                    self.dictForDetails.removeValue(forKey: "image")
                    self.tblView.delegate = self
                    self.tblView.dataSource = self
                    self.tblView.reloadData()
                    self.tblView.isHidden = false
                    self.lblForAddPicture.isHidden = true
                    self.lblForDetail.isHidden = true
                } else {
                    self.showAlert(with: "No data was Found")
                    self.resetData()
                }
            }
            
        }
    }
}

extension HomeViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dictForDetails.keys.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tblView.dequeueReusableCell(withIdentifier: "ImageDetailTVC", for: indexPath) as! ImageDetailTVC
            cell.selectionStyle = .none
            cell.lblForTitle.text = "\(Array(dictForDetails)[indexPath.row].key.capitalized) :"
            if let arrValue = Array(dictForDetails)[indexPath.row].value as? Array<Any>, arrValue.count > 0{
                if let str = arrValue[1] as? String{
                    cell.lblForDescription.text = str
                } else if let str = arrValue[1] as? Int {
                    let strValue = String(str)
                    cell.lblForDescription.text = strValue
                }
            }
            return cell
        }
    
}
extension HomeViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch collectionView.tag {
        case 0:
            return arrForFrontImage.count < 4 ? (arrForFrontImage.count + 1) : arrForFrontImage.count
        default:
            return arrForBackImage.count < 4 ? (arrForBackImage.count + 1) : arrForBackImage.count
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch collectionView.tag {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeImageCVC", for: indexPath) as! HomeImageCVC
            cell.btnForAdd.tag = indexPath.section
            cell.btnForCross.tag = indexPath.section
            
            cell.btnForAdd.addTarget(self, action: #selector(actionForAdd(sender:)), for: .touchUpInside)
            cell.btnForCross.addTarget(self, action: #selector(actionForCross(sender:)), for: .touchUpInside)
            cell.btnForAdd.isHidden = false
            if arrForFrontImage.count != 0{
                if indexPath.item == arrForFrontImage.count {
                    cell.btnForAdd.isHidden = false
                    cell.btnForCross.isHidden = true
                    cell.imgView.isHidden = true
                }else{
                    cell.imgView.isHidden = false
                    cell.btnForCross.isHidden = false
                    cell.btnForAdd.isHidden = true
                    cell.imgView.image = arrForFrontImage[indexPath.item]
                }
            }else{
                cell.imgView.isHidden = true
                cell.btnForCross.isHidden = true
            }
            return cell
            
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeImageCVC", for: indexPath) as! HomeImageCVC
            cell.btnForAdd.tag = indexPath.section
            cell.btnForCross.tag = indexPath.section
            
            cell.btnForAdd.addTarget(self, action: #selector(actionForAdd1(sender:)), for: .touchUpInside)
            cell.btnForCross.addTarget(self, action: #selector(actionForCross1(sender:)), for: .touchUpInside)
            cell.btnForAdd.isHidden = false
            if arrForBackImage.count != 0{
                if indexPath.item == arrForBackImage.count {
                    cell.btnForAdd.isHidden = false
                    cell.btnForCross.isHidden = true
                    cell.imgView.isHidden = true
                }else{
                    cell.imgView.isHidden = false
                    cell.btnForCross.isHidden = false
                    cell.btnForAdd.isHidden = true
                    cell.imgView.image = arrForBackImage[indexPath.item]
                }
            }else{
                cell.imgView.isHidden = true
                cell.btnForCross.isHidden = true
            }
            
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 95, height: 95)
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch collectionView.tag {
        case 0:
            self.showImageInLarge(image: arrForFrontImage[indexPath.item])
        default:
            self.showImageInLarge(image: arrForBackImage[indexPath.item])
        }
    }
    @objc func actionForAdd(sender:UIButton){
        CameraHandler.shared.showActionSheet(on: self) { (image) in
            self.arrForFrontImage.append(image ?? UIImage())
            self.collectionViewForFront.reloadData()
            
        }
        
    }
    @objc func actionForCross(sender:UIButton){
        if sender.tag < arrForFrontImage.count {
            arrForFrontImage.remove(at: sender.tag)
            collectionViewForFront.reloadData()
        }
    }
    @objc func actionForAdd1(sender:UIButton){
        CameraHandler.shared.showActionSheet(on: self) { (image) in
            self.arrForBackImage.append(image ?? UIImage())
            self.collectionViewForBack.reloadData()
            
        }
    }
    @objc func actionForCross1(sender:UIButton){
        if sender.tag < arrForBackImage.count {
            arrForBackImage.remove(at: sender.tag)
            collectionViewForBack.reloadData()
        }
    }
    
    func showImageInLarge(image:UIImage)  {
        
        let controller = EnlargeViewController.instantiateViewController()
        controller.image = image
        navigationController?.addChild(controller)
        controller.navigationItem.hidesBackButton = true
        controller.view?.frame = (navigationController?.view?.frame)!
        controller.hidesBottomBarWhenPushed = true
       
        navigationController?.view.window?.addSubview((controller.view)!)
        controller.dismissVCCompletion { result  in
            if result == 0{
                
            }
        }
    }
    
}
