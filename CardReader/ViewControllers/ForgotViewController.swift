//
//  ForgotViewController.swift
//  CardReader
//
//

import UIKit

class ForgotViewController: BaseViewController {

    class func instantiateViewController() -> ForgotViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let vc = storyboard.instantiateViewController(withIdentifier: String(describing: ForgotViewController.self)) as! ForgotViewController
        return vc
    }
    
    @IBOutlet weak var emailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.hideNavigationBar(true, animated: false)
    }
    
    @IBAction func resetAction(_ sender:UIButton) {
        guard let email = self.emailTextField.text, !email.isEmpty else {
            return SwiftAlert.show(self, title: "", message: "Please enter email")
        }
        
        guard Validator.email(email).validate else {
            return SwiftAlert.show(self, title: "", message: "Please enter valid email")
        }
        
        UserService().forgotPassword(email: email) { (result) in
            if result != nil{
                DispatchQueue.main.async {
                    if let dict = result as? Dictionary<String,Any>{
                        SwiftAlert.show(self, title: "" , message: dict["message"] as? String ?? "") { (ok) in
                            self.navigationController?.popViewController(animated: true)
                        }
                    }
                }
            }
        }
      
    }


}
