//
//  HomeImageCVC.swift
//  CardReader
//
//  Created by Ritesh Chopra on 3/1/21.
//

import UIKit

class HomeImageCVC: UICollectionViewCell {
    @IBOutlet weak var viewForBack: UIView!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var btnForAdd: UIButton!
    @IBOutlet weak var btnForCross: UIButton!
    
    
    override  func awakeFromNib() {
        super.awakeFromNib()
        self.viewForBack.layer.borderWidth = 2
        self.viewForBack.layer.borderColor = UIColor.black.cgColor
        self.viewForBack.layer.cornerRadius = 8
        //self.viewForBack.clipsToBounds = true
        self.viewForBack.layer.masksToBounds = true
        
        self.btnForCross.layer.cornerRadius = 12
        self.btnForCross.layer.borderWidth = 1
        self.btnForCross.layer.borderColor = UIColor.red.cgColor
        //self.btnForCross.clipsToBounds = true
        self.btnForCross.layer.masksToBounds = true
    }
}

