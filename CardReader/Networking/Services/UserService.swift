//
//  UserService.swift
//
//  Created by Ritesh Chopra on 01/08/20.

import Foundation
import UIKit

public class UserService {
    
    var headers: HTTPHeaders {
        return ["Accept": "application/json"]
        
    }
    
    var topView:BaseViewController? {
        return (UIApplication.topViewController() as? BaseViewController)
    }
    
    func isSuccess(response:Any?) -> Bool {
        
        if let dict = response as? Dictionary<String,Any>{
            if dict["code"] as? Int ?? 0 == 200 {
                return true
            } else {
                return false
            }
        }
        return false
    }
    func errorMessage(response:Any?) -> String {
        
        if let dict = response as? Dictionary<String,Any>{
            return dict["message"] as? String ?? ""
        }
        return ""
    }
    
    func signUp(username:String,
                email:String,
                password:String,
                first_name:String,
                last_name:String,
                device_type:String,
                device_token:String,
                _ completion:@escaping (Any?) -> Void) {
        
        
        let param :Parameters = ["username":username,"email":email,"password":password,"first_name":first_name,"last_name":last_name,"device_type":device_type,"device_token":device_token]
        self.topView?.showLoader()
        
        Router.data(Endpoints.api("signup/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    completion(response)
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    
    func login(username:String,
               password:String,
               _ completion:@escaping (UserResponse?) -> Void) {
        
        let param :Parameters = ["username":username,"password":password]
        self.topView?.showLoader()
        Router.data(Endpoints.api("login/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    if let data = (response as? Dictionary<String,Any>)?["data"] as? Dictionary<String,Any>,
                       let user = UserResponse(dictionary: data){
                        completion(user)
                    } else {
                        completion(nil)
                    }
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    func forgotPassword(email:String,
                        _ completion:@escaping (Any?) -> Void) {
        
        let param :Parameters = ["email":email]
        self.topView?.showLoader()
        Router.data(Endpoints.api("request-reset-email/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    completion(response)
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    
    func logOut( _ completion:@escaping (Any?) -> Void) {
        
        let param :Parameters = ["":""]
        self.topView?.showLoader()
        Router.data(Endpoints.api("logout/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).authorize(AppInstance.shared.user?.token ?? "").response(completion: { (result) in
            self.topView?.hideLoader()
            completion(nil)
        })
    }
    
    // Upload file to server
    func uploadFile(images:[UIImage?],imagesBack:[UIImage?],
                    _ completion:@escaping (Any?) -> Void) {
        
        self.topView?.showLoader()
        
        Router.upload(Endpoints.api("upload-image/"), filename: "image.jpg", name: "image_path", imageArray: images, imageBackArray: imagesBack , parameters: ["":""]).authorize(AppInstance.shared.user?.token ?? "")
            .response { (result) in
                
                switch result {
                case .Success(let response):
                    self.topView?.hideLoader()
                    if self.isSuccess(response: response) {
                        completion(response)
                    } else {
                        DispatchQueue.main.async {
                            self.topView?.showAlert(with: self.errorMessage(response: response))
                        }
                        completion(nil)
                    }
                case .Error(let error):
                    self.topView?.hideLoader()
                    self.topView?.showAlert(with: error.localizedDescription)
                    completion(nil)
                }
            }
    }
    func imageDetail(image_id:String,
                     _ completion:@escaping (Any?) -> Void) {
        
        let param :Parameters = ["user_id":AppInstance.shared.user?.user?.id ?? "","image_id":image_id]
        
        self.topView?.showLoader()
        
        Router.data(Endpoints.api("image-details/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).authorize(AppInstance.shared.user?.token ?? "").response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    completion(response)
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    
    func changePassword(old_password:String,
                        new_password:String,
                        _ completion:@escaping (Any?) -> Void) {
        
        let param :Parameters = ["old_password":old_password,"new_password":new_password]
        self.topView?.showLoader()
        
        Router.data(Endpoints.api("password-change/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).authorize(AppInstance.shared.user?.token ?? "").response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    completion(response)
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    
    func updateProfile(first_name:String,
                       last_name:String,
                       _ completion:@escaping (Any?) -> Void) {
        
        
        let param :Parameters = ["first_name":first_name,"last_name":last_name]
        self.topView?.showLoader()
        
        Router.data(Endpoints.api("update-profile/"), method: .post, parameters: param, encoder: JSONEncoding.default).headers(headers).authorize(AppInstance.shared.user?.token ?? "").response(completion: { (result) in
            switch result {
            case .Success(let response):
                self.topView?.hideLoader()
                if self.isSuccess(response: response) {
                    completion(response)
                } else {
                    DispatchQueue.main.async {
                        self.topView?.showAlert(with: self.errorMessage(response: response))
                    }
                    completion(nil)
                }
            case .Error(let error):
                self.topView?.hideLoader()
                self.topView?.showAlert(with: error.localizedDescription)
                completion(nil)
            }
        })
    }
    
}



