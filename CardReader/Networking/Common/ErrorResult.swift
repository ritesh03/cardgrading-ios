

import Foundation


public struct CustomError: Error {
    
    private let message: String
    
    var localizedDescription: String {
        return message
    }
    
    init(error: StringError) {
        self.message = error.localizedDescription
    }
}

public enum StringError: Error {
   
    case jsonConversionFailure
    case jsonParsingFailure
    case missingURL
    case encodingFailed
    case unauthorized
    case unknownErrorr
   
    var localizedDescription: String {
        switch self {
        case .jsonParsingFailure: return "JSON Parsing Failure"
        case .jsonConversionFailure: return "JSON Conversion Failure"
        case .missingURL: return "ERROR: URL Missing"
        case .encodingFailed: return "Parameter Encoding Failed"
        case .unauthorized: return "Unauthorized Request"
        case .unknownErrorr: return "Unknown Errorr"
        }
    }
}
