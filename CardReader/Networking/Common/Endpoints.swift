
import Foundation


struct Url {
    static let Base = "http://35.165.235.204:8003"
}

protocol EndpointType {
    
    var baseURL: URL { get }
    var path: String { get }
}

public enum Endpoints {
    case api(_ name: String)
}

extension Endpoints: EndpointType {
        
    var baseURL: URL {
        return URL(string: Url.Base)!
    }
    
    var path: String {
        switch self {
        case .api(let value):
            return "/api/\(value)"
        }
    }
}
