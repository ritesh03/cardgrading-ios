

import Foundation

public typealias HTTPHeaders = Dictionary<String, String>

public enum HTTPMethod : String {
    
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
    
    public var name:String {
        return self.rawValue
    }
}

